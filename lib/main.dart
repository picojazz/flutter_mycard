import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'dart:async';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  List<Marker> markers = [];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    markers.add(Marker(
        markerId: MarkerId('Amadou'),
        draggable: true,
        infoWindow: InfoWindow(title: " Ouakam ,cité assemblée vila 164"),
        position: LatLng(14.732042, -17.494322)));
  }

  //Completer<GoogleMapController> _controller = Completer();
  GoogleMapController mapController;

  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(14.732042, -17.494322),
    zoom: 17.0,
  );

  static final CameraPosition _kLake = CameraPosition(
      bearing: 192.8334901395799,
      target: LatLng(37.43296265331129, -122.08832357078792),
      tilt: 59.440717697143555,
      zoom: 19.151926040649414);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      backgroundColor: Colors.brown[400],
      body: SafeArea(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 20.0,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  width: 20.0,
                ),
                CircleAvatar(
                  radius: 50.0,
                  backgroundImage: AssetImage("images/picojazz.jpeg"),
                ),
                SizedBox(
                  width: 20.0,
                ),
                Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 15.0),
                      child: Text(
                        "Amadou Birane DIOP",
                        style: TextStyle(
                          fontSize: 35.0,
                          fontFamily: 'Odibee',
                          color: Colors.white,
                        ),
                      ),
                    ),
                    Text(
                      "Développeur Informatique",
                      style: TextStyle(
                        fontSize: 17.0,
                        fontFamily: 'shadow',
                        color: Colors.brown.shade100,
                      ),
                    ),
                  ],
                )
              ],
            ),
            SizedBox(
              width: 120.0,
              child: Divider(
                color: Colors.white,
              ),
            ),
            Card(
              margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 40.0),
              color: Colors.white,
              child: ListTile(
                  leading: Icon(
                    Icons.phone,
                    color: Colors.brown[400],
                  ),
                  title: Text(
                    '+ 221 77 951 89 58',
                    style: TextStyle(fontSize: 15.0, color: Colors.brown[400]),
                  )),
            ),
            Card(
              margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 40.0),
              color: Colors.white,
              child: ListTile(
                  leading: Icon(
                    Icons.email,
                    color: Colors.brown[400],
                  ),
                  title: Text(
                    'picojazzz@gmail.com',
                    style: TextStyle(fontSize: 15.0, color: Colors.brown[400]),
                  )),
            ),
            SizedBox(
              width: 120.0,
              child: Divider(
                color: Colors.white,
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10.0),
              width: double.infinity,
              height: 200.0,
              child: Stack(
                children: <Widget>[
                  GoogleMap(
                    mapType: MapType.hybrid,
                    initialCameraPosition: _kGooglePlex,
                    onMapCreated: (GoogleMapController controller) {
                      //_controller.complete(controller);
                      mapController = controller;
                    },
                    markers: Set.from(markers),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    ));
  }
}
